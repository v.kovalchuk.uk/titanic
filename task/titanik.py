import pandas as pd
import numpy as np

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titanic_dataframe()

    # Extract titles from the 'Name' column
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.')

    # Define the list of titles to consider
    titles_to_consider = ["Mr", "Mrs", "Miss"]

    results = []

    for title in titles_to_consider:
        median_age = int(np.nanmedian(df.loc[df['Title'] == title, 'Age']))
        missing_count = df.loc[(df['Title'] == title) & df['Age'].isnull()].shape[0]
        results.append((title, missing_count, median_age))

        # Fill missing values in the 'Age' column with the corresponding median values
        df.loc[(df['Title'] == title) & df['Age'].isnull(), 'Age'] = median_age

    # Drop the 'Title' column if you no longer need it
    df = df.drop('Title', axis=1)

    return results

# Example usage:
filled_values = get_filled()
print(filled_values)